import {CircularStdBoldText} from '../CircularStdText';
import {StyleSheet, TouchableOpacity} from 'react-native';
import React, {FC, useState} from 'react';
import styled from '@emotion/native';
import {EyeSVG} from '../../assets/svg/EyeSVG';

const EmailInput = styled.TextInput({
  width: '100%',
  color: 'rgba(56, 79, 125, 0.8)',
});

const PasswordInput = styled.TextInput({
  width: '90%',
  color: 'rgba(56, 79, 125, 0.8)',
});

const Container = styled.View({
  flexDirection: 'row',
  backgroundColor: '#fff',
  marginBottom: 20,
  borderRadius: 8,
  paddingHorizontal: 20,
  marginTop: 6,
  alignItems: 'center',
  height: 50,
  justifyContent: 'space-between',
  position: 'relative',
  borderBottomWidth: 3,
});

const styles = StyleSheet.create({
  signInTitle: {fontSize: 32, marginBottom: 90},
  label: {
    textTransform: 'uppercase',
    letterSpacing: 1,
    color: 'rgba(68, 89, 132, 0.3)',
  },
  buttonText: {
    textTransform: 'uppercase',
    color: '#fff',
    letterSpacing: 2,
  },
});

interface IInput {
  label: string;
  value: string;
  onChangeText: React.Dispatch<React.SetStateAction<string>>;
  error: boolean;
}

export const InputItem: FC<IInput> = ({
  label,
  value,
  onChangeText,
  error,
}) => {
  const [securePassword, setSecurePassword] = useState<boolean>(true);

  return (
    <>
      <CircularStdBoldText style={styles.label}>{label}</CircularStdBoldText>
      <Container style={{borderBottomColor: error ? '#D45E5E' : '#00D23A'}}>
        {label === 'Email' ? (
          <EmailInput
            value={value}
            onChangeText={text => onChangeText(text)}
            placeholder="Email"
            placeholderTextColor="#A5B0C4"
          />
        ) : (
          <>
            <PasswordInput
              value={value}
              onChangeText={text => onChangeText(text)}
              placeholder="Password"
              secureTextEntry={securePassword}
              placeholderTextColor="#A5B0C4"
            />
            <TouchableOpacity onPress={() => setSecurePassword(prev => !prev)}>
              <EyeSVG />
            </TouchableOpacity>
          </>
        )}
      </Container>
    </>
  );
};
