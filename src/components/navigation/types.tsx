import {BottomTabScreenProps} from '@react-navigation/bottom-tabs';
import {
  CompositeScreenProps,
  NavigatorScreenParams,
} from '@react-navigation/native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';

export type AppStackParamList = {
  BooksStack: NavigatorScreenParams<BooksStackParamList>;
  Settings: undefined;
};

export type AppStackScreenProps<T extends keyof AppStackParamList> =
  BottomTabScreenProps<AppStackParamList, T>;

export type BooksStackParamList = {
  Books: undefined;
  Book: {bookId: number};
};

export type BooksStackScreenProps<T extends keyof BooksStackParamList> =
  CompositeScreenProps<
    NativeStackScreenProps<BooksStackParamList, T>,
    AppStackScreenProps<keyof AppStackParamList>
  >;

declare global {
  namespace ReactNavigation {
    interface RootParamList extends AppStackParamList {}
  }
}
