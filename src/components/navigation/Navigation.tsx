import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import LogInScreen from '../../screens/LogInScreen';
import AppStack from './AppStack';
import {useAppSelector} from '../../app/hooks/hooks';

export default function Navigation() {
  const isLoggedIn: boolean = useAppSelector(state => state.user.isLoggedIn);

  return (
    <NavigationContainer>
      {isLoggedIn ? <AppStack /> : <LogInScreen />}
    </NavigationContainer>
  );
}
