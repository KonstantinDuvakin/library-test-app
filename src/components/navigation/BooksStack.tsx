import React from 'react';
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import Books from '../../screens/AppStack/BooksStack/Books';
import Book from '../../screens/AppStack/BooksStack/Book';
import {TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {BackArrowSVG} from '../../assets/svg/BackArrowSVG';
import {BooksStackParamList, BooksStackScreenProps} from './types';

const BooksScreens = createNativeStackNavigator<BooksStackParamList>();

export default function BooksStack() {
  const navigation =
    useNavigation<BooksStackScreenProps<'Book'>['navigation']>();

  const screenOptions: NativeStackNavigationOptions = {
    headerShadowVisible: false,
    headerTitleAlign: 'center',
    headerStyle: {
      backgroundColor: '#D45E5E',
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color: '#fff',
    },
    headerBackVisible: false,
  };

  return (
    <BooksScreens.Navigator
      initialRouteName="Books"
      screenOptions={screenOptions}>
      <BooksScreens.Screen
        name="Books"
        component={Books}
        options={{title: 'Books'}}
      />
      <BooksScreens.Screen
        name="Book"
        component={Book}
        options={{
          title: 'Book',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Books')}>
              <BackArrowSVG />
            </TouchableOpacity>
          ),
        }}
      />
    </BooksScreens.Navigator>
  );
}
