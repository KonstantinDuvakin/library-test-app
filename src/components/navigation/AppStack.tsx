import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Settings from '../../screens/AppStack/Settings';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import BooksStack from './BooksStack';
import type {BottomTabNavigationOptions} from '@react-navigation/bottom-tabs';
import {HomeSvg} from '../../assets/svg/HomeSVG';
import {ProfileSvg} from '../../assets/svg/ProfileSVG';
import {AppStackParamList} from './types';

const App = createBottomTabNavigator<AppStackParamList>();

export default function AppStack() {
  const {bottom} = useSafeAreaInsets();

  const screenOptions: BottomTabNavigationOptions = {
    headerShadowVisible: false,
    headerTitleAlign: 'center',
    headerStyle: {
      backgroundColor: '#D45E5E',
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color: '#fff',
    },
    tabBarStyle: {
      backgroundColor: '#fff',
      paddingHorizontal: '25%',
      height: bottom + 50,
      paddingBottom: bottom,
    },
    tabBarItemStyle: {
      width: 80,
    },
  };

  return (
    <App.Navigator initialRouteName="BooksStack" screenOptions={screenOptions}>
      <App.Screen
        name="BooksStack"
        component={BooksStack}
        options={{
          title: 'Books',
          headerShown: false,
          tabBarIcon: ({color}) => <HomeSvg color={color} />,
          tabBarActiveTintColor: '#384F7D',
          tabBarInactiveTintColor: '#A5B0C4',
          tabBarLabelStyle: {
            fontFamily: 'CircularStd-Book',
          },
        }}
      />
      <App.Screen
        name="Settings"
        component={Settings}
        options={{
          tabBarIcon: ({color}) => <ProfileSvg color={color} />,
          tabBarActiveTintColor: '#384F7D',
          tabBarInactiveTintColor: '#A5B0C4',
          tabBarLabelStyle: {
            fontFamily: 'CircularStd-Book',
          },
        }}
      />
    </App.Navigator>
  );
}
