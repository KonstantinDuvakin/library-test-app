import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {CircularStdText} from '../CircularStdText';
import {useAppDispatch} from '../../app/hooks/hooks';
import {logOutUser} from '../../app/slices/userSlice';
import styled from '@emotion/native';

const Container = styled.TouchableOpacity({
  width: '100%',
  paddingHorizontal: 30,
  paddingVertical: 17,
  borderBottomWidth: 1,
  borderBottomColor: 'rgba(56, 79, 125, 0.1)',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
})

const styles = StyleSheet.create({
  title: {
    color: '#D45E5E',
    fontSize: 16,
    marginLeft: 10,
  },
})

export const LogoutItem = () => {
  const dispatch = useAppDispatch();

  const logOut = () => {
    dispatch(logOutUser());
  };

  return (
    <Container
      activeOpacity={0.5}
      onPress={logOut}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <CircularStdText
          style={styles.title}>
          Logout
        </CircularStdText>
      </View>
    </Container>
  );
};

export default LogoutItem;
