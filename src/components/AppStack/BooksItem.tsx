import {CircularStdText} from '../CircularStdText';
import {StyleSheet, View} from 'react-native';
import React, {FC} from 'react';
import {IBook} from '../../screens/AppStack/BooksStack/Books';
import styled from '@emotion/native';
import {StarSvg} from '../../assets/svg/StarSVG';

const Container = styled.TouchableOpacity({
  width: '100%',
  padding: 20,
  backgroundColor: '#fff',
  borderRadius: 8,
  shadowColor: 'rgba(71, 55, 255, 0.8)',
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity: 0.2,
  shadowRadius: 5,
  elevation: 2,
  marginBottom: 15,
  flexDirection: 'row',
});

const Image = styled.Image({width: 80, height: 120, marginRight: 20});

const BookInfo = styled.View({flex: 1, justifyContent: 'space-between'});

const styles = StyleSheet.create({
  bookName: {fontSize: 18, color: '#384F7D'},
  authorName: {color: 'rgba(56, 79, 125, 0.8)'},
  ratingView: {flexDirection: 'row'},
});

interface Props {
  book: IBook;
  handlePress: (id: number) => void;
}

export const BooksItem: FC<Props> = ({book, handlePress}) => {
  const rating = (): JSX.Element[] => {
    const renderRating: JSX.Element[] = [];
    const rat = Math.floor(book.rating);
    for (let i = 0; i < rat; i++) {
      renderRating.push(<StarSvg key={i} color="#FFD381" />);
    }
    for (let i = rat; i < 5; i++) {
      renderRating.push(
        <StarSvg key={i} color="rgba(56, 79, 125, 0.65)" opacity="0.65" />,
      );
    }
    return renderRating;
  };

  return (
    <Container activeOpacity={0.5} onPress={() => handlePress(book.id)}>
      <Image source={{uri: book.imgPath}} />
      <BookInfo>
        <View>
          <CircularStdText style={styles.bookName}>{book.name}</CircularStdText>
          <CircularStdText style={styles.authorName}>
            {book.author}
          </CircularStdText>
        </View>
        <View style={styles.ratingView}>{rating()}</View>
      </BookInfo>
    </Container>
  );
};
