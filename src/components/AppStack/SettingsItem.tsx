import React, {FC} from 'react';
import {StyleSheet, View} from 'react-native';
import {CircularStdText} from '../CircularStdText';
import {ArrowRightSvg} from '../../assets/svg/ArrowRightSVG';
import styled from '@emotion/native';

const Container = styled.TouchableOpacity({
  width: '100%',
  paddingHorizontal: 30,
  paddingVertical: 17,
  borderBottomWidth: 1,
  borderBottomColor: 'rgba(56, 79, 125, 0.1)',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const TitleContainer = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
});

const styles = StyleSheet.create({
  iconView: {width: 15},
  title: {
    color: 'rgba(68, 89, 132, 0.8)',
    fontSize: 16,
    marginLeft: 10,
  },
});

interface Props {
  item: [JSX.Element, string];
}

export const SettingsItem: FC<Props> = ({item}) => {
  return (
    <Container activeOpacity={0.5}>
      <TitleContainer>
        <View style={styles.iconView}>{item[0]}</View>
        <CircularStdText style={styles.title}>{item[1]}</CircularStdText>
      </TitleContainer>
      <ArrowRightSvg />
    </Container>
  );
};

export default SettingsItem;
