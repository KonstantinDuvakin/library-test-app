import React, {FC} from 'react';
import styled from '@emotion/native';

interface Props {
  children: JSX.Element;
}

const Container = styled.View({
  flex: 1,
  alignItems: 'center',
  position: 'relative',
  backgroundColor: '#F0F4FD',
});

const Header = styled.View({
  width: 100,
  height: 100,
  backgroundColor: '#D45E5E',
  borderRadius: 50,
  transform: [{scaleY: 3}, {scaleX: 8}],
  position: 'absolute',
  top: -150,
});

export const AppContainer: FC<Props> = ({children}) => {
  return (
    <Container>
      <Header />
      {children}
    </Container>
  );
};
