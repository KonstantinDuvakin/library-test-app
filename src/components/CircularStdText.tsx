import React, {FC} from 'react';
import {Text} from 'react-native';

interface Style {
  [prop: string]: string | number;
}

interface Props {
  children: string | number | undefined;
  style?: Style;
  numberOfLines?: number;
}

export const CircularStdText: FC<Props> = ({
  style,
  children,
  numberOfLines = 0,
}) => {
  return (
    <Text
      style={[{fontFamily: 'CircularStd-Book', color: '#384F7D'}, style]}
      numberOfLines={numberOfLines}>
      {children}
    </Text>
  );
};

export const CircularStdBoldText: FC<Props> = ({
  style,
  children,
  numberOfLines = 0,
}) => {
  return (
    <Text
      style={[{fontFamily: 'CircularStd-Bold', color: '#384F7D'}, style]}
      numberOfLines={numberOfLines}>
      {children}
    </Text>
  );
};
