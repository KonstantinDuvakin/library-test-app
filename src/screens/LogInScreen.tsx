import React, {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Keyboard,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import styled from '@emotion/native';
import {CircularStdBoldText} from '../components/CircularStdText';
import {useAppDispatch} from '../app/hooks/hooks';
import {InputItem} from '../components/LogInScreen/InputItem';
import {logInUser} from '../app/slices/userSlice';
import LinearGradient from 'react-native-linear-gradient';

const StyledSafeAreaView = styled.SafeAreaView({
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'flex-start',
  backgroundColor: '#F0F4FD',
  position: 'relative',
});

const Button = styled.TouchableOpacity({
  width: '100%',
  paddingVertical: 20,
  alignItems: 'center',
  backgroundColor: '#6790FB',
  borderRadius: 8,
  marginBottom: 100,
  marginTop: 130,
});

const styles = StyleSheet.create({
  signInTitle: {fontSize: 32, marginBottom: 90},
  label: {
    textTransform: 'uppercase',
    letterSpacing: 1,
    color: 'rgba(68, 89, 132, 0.3)',
  },
  buttonText: {
    textTransform: 'uppercase',
    color: '#fff',
    letterSpacing: 2,
  },
  animatedView: {width: '100%', paddingHorizontal: 30},
  bigCircle: {
    width: 300,
    height: 300,
    borderRadius: 150,
    position: 'absolute',
    left: -100,
    top: -150,
  },
  mediumCircle: {
    width: 136,
    height: 136,
    borderRadius: 68,
    position: 'absolute',
    right: -40,
    top: '10%',
  },
  smallCircle: {
    width: 60,
    height: 60,
    borderRadius: 30,
    position: 'absolute',
    right: 100,
    top: '40%',
  },
});

function LogInScreen() {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errorEmail, setErrorEmail] = useState<boolean>(false);
  const [errorPassword, setErrorPassword] = useState<boolean>(false);

  const dispatch = useAppDispatch();

  const validateEmail = (emailString: string) => {
    return !!emailString.match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );
  };

  const conditionEmail =
    validateEmail(email.trim()) &&
    email.trim() === 'test@test.ru';

  const conditionPassword =
    password.trim().length > 4 &&
    password.trim() === '12345';

  const validate = (): void => {
    if (!conditionEmail) {
      setErrorEmail(true);
    } else if (!conditionPassword) {
      setErrorPassword(true);
    } else {
      dispatch(logInUser({email, password}));
      setErrorEmail(false);
      setErrorPassword(false);
    }
  };

  const animation = useRef(new Animated.Value(0)).current;

  const _keyboardWillShow = () => {
    Animated.timing(animation, {
      toValue: -50,
      duration: 50,
      useNativeDriver: true,
    }).start();
  };

  const _keyboardWillHide = () => {
    Animated.timing(animation, {
      toValue: 0,
      duration: 0,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardWillShow);
    Keyboard.addListener('keyboardDidHide', _keyboardWillHide);

    // cleanup function
    return () => {
      Keyboard.addListener('keyboardDidShow', _keyboardWillShow).remove();
      Keyboard.addListener('keyboardDidShow', _keyboardWillHide).remove();
    };
  }, []);

  return (
    <StyledSafeAreaView>
      <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#606CFF', '#79ABFC', '#FFFFFF']}
                      style={styles.bigCircle}/>
      <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#FEB665', '#F66EB4']}
                      style={styles.mediumCircle}/>
      <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 0}} colors={['#E084F1', '#2DEEF9']}
                      style={styles.smallCircle}/>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <Animated.View
          style={[styles.animatedView, {transform: [{translateY: animation}]}]}>
          <CircularStdBoldText style={styles.signInTitle}>
            Sign In
          </CircularStdBoldText>
          <InputItem label="Email" value={email} onChangeText={setEmail} error={errorEmail}/>
          <InputItem label="Password" value={password} onChangeText={setPassword} error={errorPassword}/>
          <Button onPress={validate}>
            <CircularStdBoldText
              style={styles.buttonText}>
              Sign In
            </CircularStdBoldText>
          </Button>
        </Animated.View></TouchableWithoutFeedback>
    </StyledSafeAreaView>
  );
}

export default LogInScreen;
