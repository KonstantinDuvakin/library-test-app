import {FlatList, StyleSheet, TextInput} from 'react-native';
import React, {useEffect, useState} from 'react';
import {AppContainer} from '../../../components/AppStack/AppContainer';
import {useNavigation} from '@react-navigation/native';
import {BooksStackScreenProps} from '../../../components/navigation/types';
import {CircularStdBoldText} from '../../../components/CircularStdText';
import {useAppDispatch, useAppSelector} from '../../../app/hooks/hooks';
import {BooksItem} from '../../../components/AppStack/BooksItem';
import {SearchSvg} from '../../../assets/svg/SearchSVG';
import styled from '@emotion/native';
import {setBooks, setMoreBooks, selectBookByName} from '../../../app/slices/booksSlice';
import {AxiosError, AxiosResponse} from 'axios';
const axios = require('axios');

export interface IBook {
  id: number;
  name: string;
  author: string;
  rating: number;
  imgPath: string;
  reviews: number;
  desc: string;
  taken?: boolean;
}

interface IApiBook {
  name: string;
  author: string;
  rating: number;
  imgPath: string;
  reviews: number;
  desc: string;
}

const Container = styled.View({flex: 1, width: '100%', paddingHorizontal: 30});

const InputView = styled.View({
  width: '100%',
  backgroundColor: '#fff',
  marginBottom: 30,
  marginTop: 20,
  paddingHorizontal: 20,
  paddingVertical: 15,
  borderRadius: 8,
  flexDirection: 'row',
  alignItems: 'center',
});

const styles = StyleSheet.create({
  input: {marginLeft: 10, flex: 1, height: 20, color: '#384F7D'},
  resultTitle: {
    color: 'rgba(56, 79, 125, 0.8)',
    textTransform: 'uppercase',
    fontSize: 12,
    marginBottom: 20,
  },
});

function Books() {
  const [input, setInput] = useState<string>('');
  const [isRefreshing, setIsRefreshing] = useState<boolean>(false);
  const navigation =
    useNavigation<BooksStackScreenProps<'Books'>['navigation']>();
  const dispatch = useAppDispatch();

  const books: IBook[] = useAppSelector(state =>
    selectBookByName(state, input),
  );

  const handlePress = (bookId: number) => {
    navigation.navigate('Book', {bookId});
  };

  const getBooks = async () => {
    setIsRefreshing(true);
    axios
      .get('https://run.mocky.io/v3/7601429b-029a-4bcf-9f49-a05c6195953d', {})
      .then(function (response: AxiosResponse<IApiBook[]>) {
        const apiBooks = response.data.map((item, index) => {
          const bookId: number = index + 1;
          const newItem = {
            id: bookId,
            ...item,
          };
          return newItem;
        });
        dispatch(setBooks([...apiBooks]));
        setIsRefreshing(false);
      })
      .catch(function (error: Error | AxiosError) {
        console.log(error);
        setIsRefreshing(false);
      });
  };

  const getMoreBooks = async () => {
    axios
      .get('https://run.mocky.io/v3/7601429b-029a-4bcf-9f49-a05c6195953d', {})
      .then(function (response: AxiosResponse<IApiBook[]>) {
        const apiBooks = response.data.map(item => {
          const bookId = Math.random();
          const newItem = {
            id: bookId,
            ...item,
          };
          return newItem;
        });
        dispatch(setMoreBooks([...apiBooks]));
      })
      .catch(function (error: Error | AxiosError) {
        console.log(error);
      });
  }

  useEffect(() => {
    getBooks();
  }, []);

  return (
    <AppContainer>
      <Container>
        <InputView>
          <SearchSvg />
          <TextInput
            style={styles.input}
            value={input}
            onChangeText={text => setInput(text)}
            placeholder="Search"
            placeholderTextColor="#A5B0C4"
          />
        </InputView>
        <CircularStdBoldText style={styles.resultTitle}>
          Results
        </CircularStdBoldText>
        <FlatList
          data={books}
          renderItem={({item}) => (
            <BooksItem book={item} handlePress={handlePress} />
          )}
          showsVerticalScrollIndicator={false}
          keyExtractor={item => item.id.toString()}
          onRefresh={getBooks}
          refreshing={isRefreshing}
          onEndReached={getMoreBooks}
        />
      </Container>
    </AppContainer>
  );
}

export default Books;
