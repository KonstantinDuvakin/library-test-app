import {Image, Text, TouchableOpacity, View} from 'react-native';
import React, { useState } from 'react';
import {AppContainer} from '../../../components/AppStack/AppContainer';
import {BooksStackScreenProps} from '../../../components/navigation/types';
import {useAppDispatch, useAppSelector} from '../../../app/hooks/hooks';
import {IBook} from './Books';
import {isBookTaken, selectBookById} from '../../../app/slices/booksSlice';
import {
  CircularStdBoldText, CircularStdText,
} from '../../../components/CircularStdText';
import {takeBook} from '../../../app/slices/booksSlice';
import styled from '@emotion/native';
import {StarSvg} from '../../../assets/svg/StarSVG';

const Container = styled.View({flex: 1, width: '100%'});

const BookContainer = styled.View({
  padding: 30,
  borderBottomWidth: 1,
  borderBottomColor: 'rgba(56, 79, 125, 0.1)',
});

const BookImage = styled.Image({width: 130, height: 200, marginRight: 20, borderRadius: 8});

const ButtonContainer = styled.View({
  alignItems: 'center',
  paddingTop: 40,
  paddingHorizontal: 30,
});

const BookInfoView = styled.View({flexDirection: 'row', marginBottom: 30});

const Button = styled.TouchableOpacity({
  width: '100%',
  alignItems: 'center',
  paddingVertical: 20,
  borderRadius: 8,
});

function Book({route}: BooksStackScreenProps<'Book'>) {
  const bookId = route.params.bookId;
  const book: IBook | undefined = useAppSelector(state =>
    selectBookById(state, bookId),
  );
  const isTaken: boolean = useAppSelector(state =>
    isBookTaken(state, bookId),
  );

  const [fullText, setFullText] = useState<number>(5);

  const dispatch = useAppDispatch();

  const buttonHandler = () => {
    dispatch(takeBook(bookId));
  };

  const showDesc = () => {
    if(fullText === 5) {
      setFullText(0);
    } else {
      setFullText(5);
    }
  }

  return (
    <AppContainer>
      <Container>
        <BookContainer>
          <BookInfoView>
            <BookImage
              source={{uri: book?.imgPath}}
            />
            <View style={{flex: 1}}>
              <View style={{flex: 3, justifyContent: 'flex-end'}}>
                <CircularStdBoldText
                  style={{color: '#384F7D', fontSize: 20}}>{book?.name}</CircularStdBoldText>
                <CircularStdText
                  style={{color: 'rgba(56, 79, 125, 0.8)', fontSize: 16}}>{book?.author}</CircularStdText>
              </View>
              <View style={{flex: 2, paddingTop: 20}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <StarSvg color="#445984" wh="10" />
                  <CircularStdBoldText
                    style={{color: '#384F7D', fontSize: 12, marginLeft: 3}}>{book?.rating}</CircularStdBoldText>
                </View>
                <CircularStdText
                  style={{color: 'rgba(56, 79, 125, 0.8)', fontSize: 10}}>{`${book?.reviews} reviews`}</CircularStdText>
              </View>
            </View>
          </BookInfoView>
          <View>
            <CircularStdText numberOfLines={fullText} style={{lineHeight: 22, marginBottom: 12}}>{book?.desc}</CircularStdText>
            <TouchableOpacity onPress={showDesc} activeOpacity={0.5}>
              <CircularStdBoldText numberOfLines={fullText}>{fullText === 5 ? 'Full Synopsis' : 'Short Synopsis'}</CircularStdBoldText>
            </TouchableOpacity>
          </View>
        </BookContainer>
        <ButtonContainer>
          <Button
            style={{
              backgroundColor: isTaken ? '#2DCB59' : '#6790FB',
            }}
            activeOpacity={0.5} onPress={buttonHandler}>
            <CircularStdBoldText
              style={{
                textTransform: 'uppercase',
                color: '#fff',
              }}>
              {isTaken ? 'return the book' : 'Take the book'}
            </CircularStdBoldText>
          </Button>
        </ButtonContainer>
      </Container>
    </AppContainer>
  );
}

export default Book;
