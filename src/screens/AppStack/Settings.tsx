import React from 'react';
import {AppContainer} from '../../components/AppStack/AppContainer';
import {ProfileSvg} from '../../assets/svg/ProfileSVG';
import SettingsItem from '../../components/AppStack/SettingsItem';
import {RingSvg} from '../../assets/svg/RingSVG';
import {LockSvg} from '../../assets/svg/LockSVG';
import {HelpCenterSVG} from '../../assets/svg/HelpCenterSVG';
import {GeneralSvg} from '../../assets/svg/GeneralSVG';
import LogoutItem from '../../components/AppStack/LogoutItem';
import styled from '@emotion/native';

const Container = styled.View({
  width: '100%',
  borderTopWidth: 1,
  borderTopColor: 'rgba(56, 79, 125, 0.1)',
  marginTop: 60,
});

function Settings() {
  const array: [JSX.Element, string][] = [
    [<ProfileSvg color={'#445984'} />, 'Account'],
    [<RingSvg />, 'Notifications'],
    [<LockSvg />, 'Privacy'],
    [<HelpCenterSVG />, 'Help Center'],
    [<GeneralSvg />, 'General'],
  ];

  return (
    <AppContainer>
      <Container>
        {array.map(item => (
          <SettingsItem key={item[1]} item={item} />
        ))}
        <LogoutItem />
      </Container>
    </AppContainer>
  );
}

export default Settings;
