import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import { RootState } from '../store/store';
import {IBook} from '../../screens/AppStack/BooksStack/Books';

interface BooksState {
  books: IBook[];
  booksTaken: number[];
}

let initialState: BooksState = {
  books: [],
  booksTaken: [],
};

export const booksSlice = createSlice({
  name: 'books',
  initialState,
  reducers: {
    setBooks: (state, action: PayloadAction<IBook[]>) => {
      state.books = action.payload;
    },
    setMoreBooks: (state, action) => {
      state.books.push(...action.payload);
    },
    takeBook: (state, action: PayloadAction<number>) => {
      if(state.booksTaken.indexOf(action.payload) === -1) {
        state.booksTaken.push(action.payload);
      } else {
        state.booksTaken.splice(state.booksTaken.indexOf(action.payload), 1);
      }
    },
  },
});

// Action creators are generated for each case reducer function
export const {setBooks, setMoreBooks, takeBook} = booksSlice.actions;

export const selectBookById = (state: RootState, bookId: number) =>
  state.books.books.find(book => book.id === bookId);

export const selectBookByName = (state: RootState, name: string) =>
  state.books.books.filter(book => book.name.toLowerCase().includes(name.toLowerCase()));

export const isBookTaken = (state: RootState, bookId: number) => state.books.booksTaken.includes(bookId)

export default booksSlice.reducer;
