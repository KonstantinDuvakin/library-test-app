import {createSlice} from '@reduxjs/toolkit';

interface IUser {
  user: {
    email: string;
    password: string;
  };
  isLoggedIn: boolean;
}

let initialState: IUser = {
  user: {
    email: '',
    password: '',
  },
  isLoggedIn: false,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logInUser: (state, action) => {
      const {email, password} = action.payload;
      state.user.email = email;
      state.user.password = password;
      state.isLoggedIn = true;
    },
    logOutUser: state => {
      state.user.email = '';
      state.user.password = '';
      state.isLoggedIn = false;
    },
  },
});

export const {logInUser, logOutUser} = userSlice.actions;

export default userSlice.reducer;
