import {combineReducers, configureStore} from '@reduxjs/toolkit';
import userReducer from '../slices/userSlice';
import booksReducer from '../slices/booksSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  persistReducer,
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';

const reducers = combineReducers({
  user: persistReducer(
    {key: 'user', storage: AsyncStorage, blacklist: []},
    userReducer,
  ),
  books: persistReducer(
    {key: 'books', storage: AsyncStorage, blacklist: []},
    booksReducer,
  ),
});

export const store = configureStore({
  reducer: reducers,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
