import React, {FC} from 'react';
import Svg, {Path} from 'react-native-svg';

interface Props {
  color: string;
  wh?: string;
  opacity?: string;
}

export const StarSvg: FC<Props> = ({wh = '16', color, opacity = '1.00'}) => {
  return (
    <Svg width={wh} height={wh} viewBox="0 0 16 16" fill="none">
      <Path
        d="M10 5.744L8 0L6 5.744H0L4.89 9.572L2.968 15.488L8 11.832L13.032 15.488L11.11 9.572L16 5.744H10Z"
        fill={color}
        fillOpacity={opacity}
      />
    </Svg>
  );
};
