import React, {FC} from 'react';
import Svg, {Path} from 'react-native-svg';

export const RingSvg = () => {
  return (
    <Svg width="16" height="16" viewBox="0 0 16 16" fill="none">
      <Path
        d="M12.5 5.296V5C12.5 2.515 10.485 0.5 8 0.5C5.515 0.5 3.5 2.515 3.5 5V5.296C3.5 6.719 2.743 8.042 1.508 8.75C0.867 9.118 0.5 9.544 0.5 10C0.5 11.381 3.858 12.5 8 12.5C12.142 12.5 15.5 11.381 15.5 10C15.5 9.544 15.133 9.118 14.492 8.75C13.257 8.043 12.5 6.719 12.5 5.296Z"
        stroke="#445984"
        strokeOpacity="0.65"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M10.292 14C9.90601 14.883 9.02501 15.5 8.00001 15.5C6.97501 15.5 6.09501 14.883 5.70801 14.001"
        stroke="#445984"
        strokeOpacity="0.65"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
