import React from 'react';
import Svg, {Path} from 'react-native-svg';

export const GeneralSvg = () => {
  return (
    <Svg width="15" height="15" viewBox="0 0 15 15" fill="none">
      <Path
        d="M7.5 5C8.05228 5 8.5 4.55228 8.5 4C8.5 3.44772 8.05228 3 7.5 3C6.94772 3 6.5 3.44772 6.5 4C6.5 4.55228 6.94772 5 7.5 5Z"
        fill="#445984"
        fillOpacity="0.65"
      />
      <Path
        d="M7.5 14.5C11.366 14.5 14.5 11.366 14.5 7.5C14.5 3.63401 11.366 0.5 7.5 0.5C3.63401 0.5 0.5 3.63401 0.5 7.5C0.5 11.366 3.63401 14.5 7.5 14.5Z"
        stroke="#445984"
        strokeOpacity="0.65"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M7.5 11.5V7.5"
        stroke="#445984"
        strokeOpacity="0.65"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
