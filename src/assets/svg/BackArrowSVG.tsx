import React from 'react';
import Svg, {Path} from 'react-native-svg';

export const BackArrowSVG = () => {
  return (
    <Svg width="21" height="16" viewBox="0 0 21 16" fill="none">
      <Path d="M21 8H1" stroke="white" stroke-miterlimit="10" />
      <Path
        d="M8 15L1 8L8 1"
        stroke="white"
        strokeMiterlimit="10"
        strokeLinecap="square"
      />
    </Svg>
  );
};
